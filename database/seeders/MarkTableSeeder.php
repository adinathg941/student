<?php

namespace Database\Seeders;

use App\Models\Mark;
use Illuminate\Database\Seeder;

class MarkTableSeeder extends Seeder
{
    public function run()
    {
        $marks = [
            [
                'id'             => 1,
                'term'           => 'One',
                'maths'          => 30,
                'science'          => 40,
                'history'          => 18,
                'name_id'       => 1,
            ],[
                'id'             => 2,
                'term'           => 'One',
                'maths'          => 18,
                'science'          => 30,
                'history'          => 35,
                'name_id'       => 1,
            ],[
                'id'             => 3,
                'term'           => 'Two',
                'maths'          => 18,
                'science'          => 30,
                'history'          => 35,
                'name_id'       => 2,
            ],
        ];

        Mark::insert($marks);
    }
}
