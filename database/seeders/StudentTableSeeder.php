<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    public function run()
    {
        $students = [
            [
                'id'             => 1,
                'name'           => 'John Doe',
                'age'          => 18,
                'gender'       => 'M',
                'reporting_teacher' => 'Katie',
            ],[
                'id'             => 2,
                'name'           => 'Mary',
                'age'          => 22,
                'gender'       => 'F',
                'reporting_teacher' => 'Max',
            ],
        ];

        Student::insert($students);
    }
}
