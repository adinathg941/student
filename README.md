# Student Management System

## Installation

To run

```bash
  composer install
  setup .env file
  php artisan migrate
  php artisan db:seed

```

## Usage/Examples

```javascript
php artisan serve


```

Credentials

```javascript
admin@admin.com:password
```
