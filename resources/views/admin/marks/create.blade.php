@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.mark.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.marks.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name_id">{{ trans('cruds.mark.fields.name') }}</label>
                <select class="form-control select2 {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name_id" id="name_id" required>
                    @foreach($names as $id => $entry)
                        <option value="{{ $id }}" {{ old('name_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mark.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.mark.fields.term') }}</label>
                <select class="form-control {{ $errors->has('term') ? 'is-invalid' : '' }}" name="term" id="term" required>
                    <option value disabled {{ old('term', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Mark::TERM_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('term', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('term'))
                    <span class="text-danger">{{ $errors->first('term') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mark.fields.term_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="maths">{{ trans('cruds.mark.fields.maths') }}</label>
                <input class="form-control {{ $errors->has('maths') ? 'is-invalid' : '' }}" type="number" name="maths" id="maths" value="{{ old('maths', '') }}" step="1" required>
                @if($errors->has('maths'))
                    <span class="text-danger">{{ $errors->first('maths') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mark.fields.maths_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="science">{{ trans('cruds.mark.fields.science') }}</label>
                <input class="form-control {{ $errors->has('science') ? 'is-invalid' : '' }}" type="number" name="science" id="science" value="{{ old('science', '') }}" step="1" required>
                @if($errors->has('science'))
                    <span class="text-danger">{{ $errors->first('science') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mark.fields.science_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="history">{{ trans('cruds.mark.fields.history') }}</label>
                <input class="form-control {{ $errors->has('history') ? 'is-invalid' : '' }}" type="number" name="history" id="history" value="{{ old('history', '') }}" step="1" required>
                @if($errors->has('history'))
                    <span class="text-danger">{{ $errors->first('history') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mark.fields.history_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection