<?php

namespace App\Http\Requests;

use App\Models\Mark;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateMarkRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name_id' => [
                'required',
                'integer',
            ],
            'term' => [
                'required',
            ],
            'maths' => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'science' => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'history' => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
