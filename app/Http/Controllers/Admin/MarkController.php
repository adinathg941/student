<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyMarkRequest;
use App\Http\Requests\StoreMarkRequest;
use App\Http\Requests\UpdateMarkRequest;
use App\Models\Mark;
use App\Models\Student;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MarkController extends Controller
{
    /**
     * @author Adinath
     * view list
     */
    public function index()
    {
        $marks = Mark::with(['name'])->get();
        return view('admin.marks.index', compact('marks'));
    }

    /**
     * @author Adinath
     * Create Mark
     */
    public function create()
    {
        

        $names = Student::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.marks.create', compact('names'));
    }

    /**
     * @author Adinath
     * Save Mark details
     */
    public function store(StoreMarkRequest $request)
    {
        $mark = Mark::create($request->all());

        return redirect()->route('admin.marks.index');
    }

    /**
     * @author Adinath
     * @param Mark
     * Edit Mark
     */
    public function edit(Mark $mark)
    {
        
        $names = Student::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $mark->load('name');

        return view('admin.marks.edit', compact('names', 'mark'));
    }

    /**
     * @author Adinath
     * Update Mark
     * @param Request,Mark
     */
    public function update(UpdateMarkRequest $request, Mark $mark)
    {
        $mark->update($request->all());

        return redirect()->route('admin.marks.index');
    }

    /**
     * @author Adinath
     * Delete Mark
     * @param Mark
     */
    public function destroy(Mark $mark)
    {
        
        $mark->delete();

        return back();
    }

    /**
     * @author Adinath
     * Delete Marks
     * @return Response
     */
    public function massDestroy(MassDestroyMarkRequest $request)
    {
        Mark::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
