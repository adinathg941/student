<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyStudentRequest;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Models\Student;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class StudentController extends Controller
{
    /**
     * @author Adinath
     * View Student
     * @param void
     * @return void
     */
    public function index()
    {
        
        $students = Student::all();

        return view('admin.students.index', compact('students'));
    }

    /**
     * @author Adinath
     * Create Student
     */
    public function create()
    {
        
        return view('admin.students.create');
    }

    /**
     * @author Adinath
     * Save Student details
     */
    public function store(StoreStudentRequest $request)
    {
        $student = Student::create($request->all());

        return redirect()->route('admin.students.index');
    }

    /**
     * @author Adinath
     * edit Student details
     * @param Student student
     */
    public function edit(Student $student)
    {
        
        return view('admin.students.edit', compact('student'));
    }

    /**
     * @author Adinath
     * update student details
     * @param Request,Student
     */
    public function update(UpdateStudentRequest $request, Student $student)
    {
        $student->update($request->all());

        return redirect()->route('admin.students.index');
    }

    /**
     * @author Adinath
     * Delete Student details
     * @param Student
     */
    public function destroy(Student $student)
    {
        
        $student->delete();

        return back();
    }

    /**
     * @author Adinath
     */
    public function massDestroy(MassDestroyStudentRequest $request)
    {
        Student::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
