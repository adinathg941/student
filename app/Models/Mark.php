<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    use HasFactory;

    public const TERM_SELECT = [
        'One' => 'One',
        'Two' => 'Two',
    ];

    public $table = 'marks';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name_id',
        'term',
        'maths',
        'science',
        'history',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * Relaton to Student Table
     */
    public function name()
    {
        return $this->belongsTo(Student::class, 'name_id');
    }
    /**
     * Date mutator
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
